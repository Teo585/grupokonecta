<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuarios extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'email','password','remember_token','Rol_idRol','created_at','updated_at'];
    public $timestamps = false;

    public function Rol()
    {
        return $this->hasOne('App\Rol','idRol');
    }

}
