<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{
    protected $table = 'clientes';
    protected $primaryKey = 'idCliente';
    protected $fillable = ['nombreCliente', 'documentoCliente','correoCliente','direccionCliente'];
    public $timestamps = false;

}
