<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use DB;



// Hash de contraseñas.
use Hash;
//Usamos el redirect para redireccionar desde php
use Redirect;
//Consultamos los permisos que tenga asignado ese usuario
include public_path().'/ajax/consultarPermisos.php';


class usuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                

        $vista = basename($_SERVER["PHP_SELF"]);
        $datos = consultarPermisos($vista);
        
        if($datos != null)
        return view('usuariosgrid', compact('datos'));
        else
        return view('accesodenegado');
        //Siempre que abra el módulo va a abrir el SCRUD
        //return view('usuariosgrid',compact('datos'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Rol = \App\rol::lists('nombreRol','idRol'); 

                                //Devolvemos a la vista esa consulta de id y name
        return view('usuarios',compact('Rol'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \App\User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => $request['password'],
            'Rol_idRol' => $request['Rol_idRol'],
            ]);
        
        return redirect('/usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {              
        //Consultamos con el id guardado para volver a cargar el formulario
        $usuarios = \App\usuarios::find($id);

        $Rol = \App\rol::lists('nombreRol','idRol'); 
        //Devolvemos a la vista esa consulta de id y name
        return view('usuarios',compact('Rol'),['usuarios'=>$usuarios]);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = \App\usuarios::find($id);
        $usuario->fill($request->all());

        $usuario->save();

        return redirect('/usuarios');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\usuarios::destroy($id);
        return redirect('/usuarios');
    }
}
