<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
//Usamos el redirect para redireccionar desde php
use Redirect;
//Consultamos los permisos que tenga asignado ese usuario
include public_path().'/ajax/consultarPermisos.php';


class rolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                

        $vista = basename($_SERVER["PHP_SELF"]);
        $datos = consultarPermisos($vista);
        
        if($datos != null)
        return view('rolgrid', compact('datos'));
        else
        return view('accesodenegado');
        //Siempre que abra el módulo va a abrir el SCRUD
        //return view('rolgrid',compact('datos'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

                                //Devolvemos a la vista esa consulta de id y name
        return view('rol',compact('tipoDocumento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \App\rol::create([
            'nombreRol' => $request['nombreRol'],
            'nombreVista' => $request['nombreVista'],
            'adicionarRol' => $request['opcionAdicionar'],
            'modificarRol' => $request['opcionModificar'],
            'eliminarRol' => $request['opcionEliminar'],
            ]);
        
        return redirect('/rol');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {              
        //Consultamos con el id guardado para volver a cargar el formulario
        $rol = \App\rol::find($id);
        //Devolvemos a la vista esa consulta de id y name
        return view('rol',['rol'=>$rol]);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $indice = array(
            'idRol' => $id);

            $data = array(
                'nombreRol' => $request['nombreRol'],
                'nombreVista' => $request['nombreVista'],
                'adicionarRol' => $request['opcionAdicionar'],
                'modificarRol' => $request['opcionModificar'],
                'eliminarRol' => $request['opcionEliminar']);


            $rol = \App\rol::updateOrCreate($indice, $data);

        return redirect('/rol');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\rol::destroy($id);
        return redirect('/rol');
    }
}
