<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//use App\Http\Requests\request;
use App\Http\Controllers\Controller;
// Hash de contraseñas.
use Hash;
use DB;
//Usamos el redirect para redireccionar desde php
use Redirect;
//Consultamos los permisos que tenga asignado ese usuario
include public_path().'/ajax/consultarPermisos.php';


class clientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                

        $vista = basename($_SERVER["PHP_SELF"]);
        $datos = consultarPermisos($vista);
        
        if($datos != null)
        return view('clientegrid', compact('datos'));
        else
        return view('accesodenegado');
        //Siempre que abra el módulo va a abrir el SCRUD
        //return view('usuariosgrid',compact('datos'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

                                
        return view('cliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {
        \App\clientes::create([
            'nombreCliente' => $request['nombreCliente'],
            'documentoCliente' => $request['documentoCliente'],
            'correoCliente' => $request['correoCliente'],
            'direccionCliente' => $request['direccionCliente'],
            ]);
        
           
      
        return redirect('/cliente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {              
        //Consultamos con el id guardado para volver a cargar el formulario
        $clientes = \App\clientes::find($id);

         
        //Devolvemos a la vista esa consulta de id y name
        return view('cliente',['clientes'=>$clientes]);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request, $id)
    {
        $clientes = \App\clientes::find($id);
        $clientes->fill($request->all());

        $clientes->save();

        return redirect('/cliente');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\clientes::destroy($id);
        return redirect('/cliente');
    }
}
