<?php

// Ponemos la ruta del login para que sea la ruta siempre principal
Route::get('/', 'Auth\AuthController@getLogin')->name('home');


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
//ruta de casos con su vista y contralador



// ---------------------------------
// Ruta de accesodenegado
// ---------------------------------
Route::get('accesodenegado', function () {
    return view('accesodenegado');
    });


// ---------------------------------
// Ruta de pagina inicial
// ---------------------------------
Route::get('paginainicial', function () {
    return view('paginainicial');
    });



//ruta de casos con su vista y contralador
Route::resource ('usuarios','usuariosController');
Route::resource ('rol','rolController');
Route::resource ('cliente','clientesController');



//Creamos la ruta para el ajax del rellenado de la tabla scrud de abogado
Route::get('datosUsuario', function()
{
    include public_path().'/ajax/datosUsuario.php';
});

Route::get('datosRol', function()
{
    include public_path().'/ajax/datosRol.php';
});
Route::get('datosClientes', function()
{
    include public_path().'/ajax/datosClientes.php';
});
?>