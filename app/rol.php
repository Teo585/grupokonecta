<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rol extends Model
{
    protected $table = 'rol';
    protected $primaryKey = 'idRol';
    protected $fillable = ['nombreRol', 'nombreVista','adicionarRol','modificarRol','eliminarRol'];
    public $timestamps = false;

    public function usuarios()
    {
        return $this->hasMany('App\usuarios','Rol_idRol');
    }

}
