<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Se crea el schema para la tabla tipos de documentos
         Schema::create('clients',function(Blueprint $table)
         {
             //Id autoincremental para la tabla
             $table->increments('id') ;
             //Nombre y apellido      
             $table->string('names',80);   
             $table->string('lastname',80);   
             /*El campo lo ponemos unsigned para luego hacer la relación*/
             $table->integer('document_type_id')->unsigned();  
             $table->string('document_number',80);  
             
             
                  //Llave foranea con document_types
            $table->foreign('document_type_id')
            ->references('id')->on('document_types')
            ->onDelete('cascade');
          
 
          
          $table->softDeletes(); //Este es para el deleted_at
          $table->timestamp('created_at')->useCurrent();
          $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
