<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Statuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Se crea el schema para la tabla tipos de documentos
         Schema::create('statuses',function(Blueprint $table)
         {
             //Id autoincremental para la tabla
             $table->increments('id') ;  
             $table->string('code',40);   
             $table->string('name',80);   
              
            
             $table->softDeletes(); //Este es para el deleted_at
             $table->timestamp('created_at')->useCurrent();
             $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
