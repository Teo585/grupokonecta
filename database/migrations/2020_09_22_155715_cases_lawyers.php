<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CasesLawyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Se crea el schema para la tabla tipos de documentos
         Schema::create('cases_lawyers',function(Blueprint $table)
         {
            //Id autoincremental para la tabla
            $table->increments('id') ;
            /*El campo lo ponemos unsigned para luego hacer la relación*/
            $table->integer('case_id')->unsigned();  
            /*El campo lo ponemos unsigned para luego hacer la relación*/
            $table->integer('lawyer_id')->unsigned();  


             
                  //Llave foranea con case
            $table->foreign('case_id')
            ->references('id')->on('cases')
            ->onDelete('cascade');
            
            //Llave foranea con case
            $table->foreign('lawyer_id')
            ->references('id')->on('lawyers')
            ->onDelete('cascade');
          
 
          
          $table->softDeletes(); //Este es para el deleted_at
          $table->timestamp('created_at')->useCurrent();
          $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
