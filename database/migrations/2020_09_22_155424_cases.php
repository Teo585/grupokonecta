<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Se crea el schema para la tabla tipos de documentos
         Schema::create('cases',function(Blueprint $table)
         {
             //Id autoincremental para la tabla
             $table->increments('id') ;
             $table->string('name',80);   
             /*El campo lo ponemos unsigned para luego hacer la relación*/
             $table->integer('client_id')->unsigned(); 
             $table->string('cost',80);   
            /*El campo lo ponemos unsigned para luego hacer la relación*/
            $table->integer('status_id')->unsigned(); 
            
    
                  //Llave foranea con clients
            $table->foreign('client_id')
            ->references('id')->on('clients')
            ->onDelete('cascade');

             //Llave foranea con status
             $table->foreign('status_id')
             ->references('id')->on('statuses')
             ->onDelete('cascade');

             $table->softDeletes(); //Este es para el deleted_at
             $table->timestamp('created_at')->useCurrent();
             $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));



          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
