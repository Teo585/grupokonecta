<?php

use Illuminate\Database\Seeder;
//importante usar el DB para las tablas
use Illuminate\Support\Facades\DB;

class document_typesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_types')->insert([
            'code' => 'AAA1',
            'name' => 'CEDULA DE CIUDADADIA'
        ]);
        DB::table('document_types')->insert([
            'code' => 'AAA2',
            'name' => 'TARJETA DE INDENTIDAD'
        ]);
        DB::table('document_types')->insert([
            'code' => 'AAA3',
            'name' => 'PASAPORTE'
        ]);

    }
}
