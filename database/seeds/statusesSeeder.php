<?php

use Illuminate\Database\Seeder;
//importante usar el DB para las tablas
use Illuminate\Support\Facades\DB;
 

class statusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'code' => 'AA1',
            'name' => 'tramite'
        ]);
        DB::table('statuses')->insert([
            'code' => 'AA2',
            'name' => 'archivado'
        ]);
        DB::table('statuses')->insert([
            'code' => 'AA3',
            'name' => 'anulado'
        ]);
        DB::table('statuses')->insert([
            'code' => 'AA4',
            'name' => 'aprobado'
        ]);
    }
}
