function validarFormulario(event)
{
	validacionConfirmarContrasenia();
		var resp = true;


		if((document.getElementById("name").value == '' ))
		{
			document.getElementById("name").style = "background-color:#F5A9A9;";
			resp = false;
		} 
		else
		{
			 document.getElementById("name").style = "background-color:white;";
		} 

		if((document.getElementById("email").value == '' ))
		{
			document.getElementById("email").style = "background-color:#F5A9A9;";
			resp = false;
		} 
		else
		{
			 document.getElementById("email").style = "background-color:white;";
		} 

		if((document.getElementById("password").value == '' ))
		{
			document.getElementById("password").style = "background-color:#F5A9A9;";
			resp = false;
		} 
		else
		{
			 document.getElementById("password").style = "background-color:white;";
		} 

		
		if((document.getElementById("remember_token").value == '' ))
		{
			document.getElementById("remember_token").style = "background-color:#F5A9A9;";
			resp = false;
		} 
		else
		{
			 document.getElementById("remember_token").style = "background-color:white;";
		} 
		


		//Validación para campos tipo chosen
		nombreCampo = 'Rol_idRol';
		
		if((document.getElementById(nombreCampo).value == '' ))
		{ 
			resp = false;

				//Muestra el mensaje campos en rojo y no deja seleccionar la nueva lista que sale 
			// El primer ID es para asignarle un alto al borde que estamos asignando
			document.getElementById(nombreCampo).style = "height:35px";
			//Se asigna el color para el borde
			document.getElementById(nombreCampo+'_chosen').style = "border:solid #d80f0f;10px";

			// se deja el chosen 
			document.getElementById(nombreCampo+'_chosen').style.display = 'block';
			//Siempre se oculta el SELECT NORMAL para que no estorbe
			document.getElementById(nombreCampo).style.display = 'none';
		} 
		else
		{      
			resp = true;
		} 
		//Si llenaron va a quitar el campo rojo 
		if(resp === true)
		{   
			// Primero se bloquea el SELECT chousen, ya que en la funcion de CEERRAR DIALOGO quitará el borde
			// Segundo se devulve un borde sin color
			document.getElementById(nombreCampo+'_chosen').style.display = 'block';
			document.getElementById(nombreCampo+'_chosen').style.border = "thin solid  rgb(255, 255, 255)";
		}
		else
		{
			//Si no llenaron  devuelve este mensaje
			alert('Debe diligenciar los campos resaltados en color rojo');
			//evitamos que el formulario se cierre
			event.preventDefault();
		}
}
//Se agrega al campo Chousen del formulario la funcion para que envie el valor
function cerrarDialogoValidacion($Value)
{
    if ($Value != '') 
    {
        //Si le dan click en otro valor en el chousen select se quita el rojo
        document.getElementById(nombreCampo).style.display = 'none';
    }
    
}


function validacionConfirmarContrasenia()
{
	if(document.getElementById("password").value == document.getElementById("remember_token").value)
	{
		//Si es igual no hacemos nada
	}
	else
	{
		//Si las contraseñas no coinciden error
		alert('La contraseña de verificación no coincide.');
		//evitamos que el formulario se cierre
		event.preventDefault();
	}
}