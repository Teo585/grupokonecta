//Java script para el menú
$(function () {

    $('.navbar-toggler').on('click', function(event) {
		event.preventDefault();
		$(this).closest('.navbar-minimal').toggleClass('open');
	})
});


function validarRedirect(tipo)
{		

	if(tipo=="usuario")
	{
		window.location = "/usuarios";
	}
	else
	if(tipo=="paginainicial")
	{
		window.location = "/paginainicial";
	}
	else
	if(tipo=="rol")
	{
		window.location = "/rol";
	}
	else
	if(tipo=="clientes")
	{
		window.location = "/cliente";
	}
	else
	if(tipo=="logout")
	{
		window.location = "/auth/logout";
	}

}