<?php
   $modificar = $_GET['modificar'];
   $eliminar = $_GET['eliminar'];


   $visibleM = '';
   $visibleE = '';
   

   if ($modificar == 1) 
       $visibleM = 'inline-block;';
   else
       $visibleM = 'none;';

   if ($eliminar == 1) 
       $visibleE = 'inline-block;';
   else
       $visibleE = 'none;';

  
    //Hacemos la consulta para traer el id y el nombre para rellenar la tabla SCRUD
    $users = DB::SELECT('SELECT *
    FROM  clientes ');


    //creo el array que va a contener todo
    $row = array();
    foreach ($users as $key => $value) 
    {  
        $row[$key][] = '<a href="cliente/'.$value->idCliente.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil" style = "display:'.$visibleM.'"></span>'.
                        '</a>&nbsp;'.
                        '<a href="cliente/'.$value->idCliente.'/edit?accion=eliminar">'.
                            '<span class="glyphicon glyphicon-trash" style = "display:'.$visibleE.'"></span>'.
                        '</a>'
                        ;

        $row[$key][] = $value->idCliente;
        $row[$key][] = $value->nombreCliente;
        $row[$key][] = $value->documentoCliente;
        $row[$key][] = $value->correoCliente;
        $row[$key][] = $value->direccionCliente;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>
