<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	@yield('clases')
	{!!Html::style('css/menu.css'); !!}
	{!!Html::style('assets/bootstrap-v3.3.5/css/bootstrap.min.css'); !!}
	
	{!!Html::script('assets/bootstrap-v3.3.5/js/bootstrap.min.js'); !!}



    <!-- IZITOAST (libreria para mensajes de alerta) -->
    {!!Html::style('assets/izitoast/dist/css/iziToast.min.css'); !!}
	{!!Html::script('assets/izitoast/dist/js/iziToast.min.js'); !!}


   <!-- Invocamos el menu js para la animación del menú -->
   {!!Html::script('js/menu.js')!!}

	<!-- titulo de la pestaña -->
	<title>Proyecto</title>
</head>


<body id='body'>

	<div id="header">
		<div id="container">
		<div class="barramenu">
			
	<!-- Div para el menú despleable -->
	<div id="DivMenuDesplegable" class="panel panel-primary" style='border:0'>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	
	<!------ Include the above in your HEAD tag ---------->

		<nav class="navbar navbar-fixed-left navbar-minimal animate" role="navigation">
			<div class="navbar-toggler animate">
				<span class="menu-icon"></span>
			</div>
			<ul class="navbar-menu animate">
				<li>
					<a  class="animate" onclick="validarRedirect('usuario')">
						<span class="desc animate"> Usuarios </span>
						<span class="fa fa-user-plus"></span>
					</a>
				</li>
				<li>
					<a  class="animate"  onclick="validarRedirect('paginainicial')">
						<span class="desc animate"> Página inicial</span>
						<span class="fa fa-home" aria-hidden="true""></span>
					</a>
				</li>
				<li>
					<a class="animate" onclick="validarRedirect('rol')">
						<span class="desc animate"> Roles de usuario</span>
						<span class="fa fa-list-alt"></span>
					</a>
				</li>
				<li>
					<a  class="animate" onclick="validarRedirect('clientes')">
						<span class="desc animate"> Clientes</span>
						<span class="fa fa-users"></span>
					</a>
				</li>
				<li>
					<a  class="animate" onclick="validarRedirect('logout')">
						<span class="desc animate"> Logout-Cerrar sesión</span>
						<span class="fa fa-sign-out"></span>
					</a>
				</li>
				
			</ul>
		</nav>



		<div id="contenedor" class="panel panel-primary"">
			<div   class="panel panel-heading">
				@yield('titulo')
			</div>
			<div  id="contenedor-fin" class="panel-body">
				@yield('content') 
			</div>
		</div>
	</div>


	<div id="footer">
	    <p>Proyecto Todos los derechos reservados</p>
	</div>
</body>
</html>