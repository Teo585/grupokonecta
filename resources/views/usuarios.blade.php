@extends('layouts.vista')
@section('titulo')<h3 id="titulo"><center>Registro de usuarios</center></h3>@stop
@section('content')
  @include('alerts.request')

  {!!Html::script('js/usuarios.js')!!}

	@if(isset($usuarios))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'eliminar')
			{!!Form::model($usuarios,['route'=>['usuarios.destroy',$usuarios->id],'method'=>'DELETE'])!!}
		@else
			{!!Form::model($usuarios,['route'=>['usuarios.update',$usuarios->id],'method'=>'PUT'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'usuarios.store','method'=>'POST'])!!}
	@endif
  

<div id='form-section' >
<input type="hidden" id="token" value="{{csrf_token()}}"/>
	<fieldset id="usuarios-form-fieldset">	
    <div class="form-group" >
          {!!Form::label('name', 'Nombre Usuario', array('class' => 'col-sm-2 control-label'))!!}
          <div class="col-sm-10" >
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-pencil" style="width: 14px;"></i>
                  </span>             
                    {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingresa el nombre del usuarios'])!!}
                </div>
          </div>
    </div>
    <div class="form-group" >
          {!!Form::label('email', 'Correo', array('class' => 'col-sm-2 control-label'))!!}
          <div class="col-sm-10" >
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-pencil" style="width: 14px;"></i>
                  </span>             
                    {!!Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa el correo del usuario'])!!}
                </div>
          </div>
    </div>
    
    <div class="form-group required" id='test'>
          {!! Form::label('password', 'Contrase&ntilde;a', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-key " style="width: 14px;"></i>
                </span>
            {!!Form::password('password',array('class'=>'form-control','placeholder'=>'Ingresa la contrase&ntilde;a'))!!}
            </div>
        </div>
    </div>

    <div class="form-group required" id='test' style="display:inline-block;">
        {!! Form::label('remember_token', 'Confirmar Contrase&ntilde;a', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-key " style="width: 14px;"></i>
            </span>
            {!!Form::password('remember_token',array('class'=>'form-control','placeholder'=>'Ingresa de nuevo la contrase&ntilde;a'))!!}
        </div>
    </div>
    
    <div class="form-group " style="display:inline-none;">
        {!!Form::label('Rol_idRol', 'Rol', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-sm-10" >
            <div class="input-group" >
            <span class="input-group-addon">
                <i class="fa fa-list" style="width: 14px;" ></i>
            </span>
            {!!Form::select('Rol_idRol',$Rol, (isset($usuarios) ? $usuarios->Rol_idRol : 0),["class" => "chosen-select form-control", "placeholder" =>"Seleccione el Rol"])!!}
            </div>
        </div>
    </div>



    


    </fieldset>
    <br>
	@if(isset($usuarios))
 		@if(isset($_GET['accion']) and $_GET['accion'] == 'eliminar')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'] )!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
 	@endif
    {!! Form::close() !!}
    



</div>
@stop