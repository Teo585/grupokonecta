@extends('layouts.vista')
@section('titulo')<h3 id="titulo"><center>Rol de usuarios</center></h3>@stop
@section('content')
  @include('alerts.request')
  {!!Html::script('js/rol.js')!!}
	@if(isset($rol))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'eliminar')
			{!!Form::model($rol,['route'=>['rol.destroy',$rol->idRol],'method'=>'DELETE'])!!}
		@else
			{!!Form::model($rol,['route'=>['rol.update',$rol->idRol],'method'=>'PUT'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'rol.store','method'=>'POST'])!!}
	@endif
  

<div id='form-section' >
<input type="hidden" id="token" value="{{csrf_token()}}"/>
	<fieldset id="rol-form-fieldset">	
    <div class="form-group" >
          {!!Form::label('nombreRol', 'Nombre rol', array('class' => 'col-sm-2 control-label'))!!}
          <div class="col-sm-10" >
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-pencil" style="width: 14px;"></i>
                  </span>             
                    {!!Form::text('nombreRol',null,['class'=>'form-control','placeholder'=>'Ingresa el nombre del rol'])!!}
                    {!! Form::hidden('validarIdGuardado', (isset($rol) ? $rol->adicionarRol : null), array('id' => 'validarIdGuardado')) !!}
                </div>
          </div>
    </div>
    <div class="form-group required" id='test'>
					{!!Form::label('nombreVista', 'Nombre vista', array('class' => 'col-sm-2 control-label'))!!}
					<div class="col-sm-10">
			            <div class="input-group">
			              	<span class="input-group-addon">
			                	<i class="fa fa-list"></i>
			              	</span>
							{!!Form::select('nombreVista',
							array(
              'todas' => 'todas',
							'usuarios' => 'usuarios',
							'clientes' => 'clientes',
							'rol' => 'rol',
							),
							(isset($rol) ? $rol->nombreVista : 0),
							['class'=>'form-control','placeholder'=>'Seleccione la vista para los permisos'])!!}
			    		</div>
          </div>
      </div>	
      <div class="container my-4">
          <hr>
          </br>
          </br>
          <div class="panel panel-default">
            <div class="panel-body">
              Seleccione los permisos para el rol a crear
            </div>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item" >
              <!-- Default checked -->
              <div class="custom-control custom-checkbox" id="opcion1">
              {!! Form::checkbox('opcion1', 1, false, ['onclick' => 'asignarValoresChulos();', 'id' => 'opcion1']) !!} 
              {!! Form::hidden('opcionAdicionar', (isset($rol) ? $rol->adicionarRol : null), array('id' => 'opcionAdicionar')) !!}
              {!! Form::hidden('opcionModificar', (isset($rol) ? $rol->modificarRol : null), array('id' => 'opcionModificar')) !!}
              {!! Form::hidden('opcionEliminar', (isset($rol) ? $rol->eliminarRol : null), array('id' => 'opcionEliminar')) !!}

                <label class="custom-control-label" for="check1">Creación</label>
                
              </div>
            </li>
            <li class="list-group-item">
              <!-- Default checked -->
              <div class="custom-control custom-checkbox" id="opcion2">
              {!! Form::checkbox('opcion2', 1, false, ['onclick' => 'asignarValoresChulos();', 'id' => 'opcion2']) !!}
                <label class="custom-control-label" for="check2">Actualización</label>
              </div>
            </li>
            <li class="list-group-item">
              <!-- Default checked -->
              <div class="custom-control custom-checkbox" id="opcion3">
              {!! Form::checkbox('opcion3', 1, false, ['onclick' => 'asignarValoresChulos();', 'id' => 'opcion3']) !!}
                <label class="custom-control-label" for="check3">Eliminación</label>
              </div>
            </li>
          </ul>
      </div>
      
    </fieldset>
    <br>
	@if(isset($rol))
 		@if(isset($_GET['accion']) and $_GET['accion'] == 'eliminar')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
 	@endif
    {!! Form::close() !!}
    

<script type="text/javascript">

function asignarValoresChulos()
{
  if($("#opcion1").prop('checked') == true)
  {
    $('#opcionAdicionar').val(1);
  }
  else
  {
    $('#opcionAdicionar').val(0);
  }
  if($("#opcion2").prop('checked') == true)
  {
    $('#opcionModificar').val(1);
  }
  else
  {
    $('#opcionModificar').val(0);
  }
  if($("#opcion3").prop('checked') == true)
  {
    $('#opcionEliminar').val(1);
  }
  else
  {
    $('#opcionEliminar').val(0);
  }
}

//Validamos los checkbox para devolverlos a su posición solo si existe un registro guardado
if(validarIdGuardado != '')
{ 
  //Antes de devolver el formulario verificamos los valores para devolver los chulos
  $(document).ready(function()
    {
      if($('#opcionAdicionar').val()!=0)
      {
        $('#opcion1').prop('checked',true);
      }
      if($('#opcionModificar').val()!=0)
      {
        $('#opcion2').prop('checked',true);
      }
      if($('#opcionEliminar').val()!=0)
      {
        $('#opcion3').prop('checked',true);
      }  
    });
}

</script>


</div>
@stop