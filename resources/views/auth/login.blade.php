@extends('layouts.acceso')

@section('content')
<center>@include('alerts.logError')</center>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

<div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div class="box">
                    <div class="shape1"></div>
                    <div class="shape2"></div>
                    <div class="shape3"></div>
                    <div class="shape4"></div>
                    <div class="shape5"></div>
                    <div class="shape6"></div>
                    <div class="shape7"></div>
                    <div class="float">
                    {!! Form::open(['route' => 'auth/login', 'class' => 'form'])!!}
                        <div class="form-group">
                        <label for="username" class="text-white">Correo:</label><br>
                        {!!Form::email('email','',["required"=>"required",'class'=> 'form-control','id'=>'nombre','placeholder'=>'Digite su correo'])!!}
                        </div>
                        <div class="form-group">
                        <label for="password" class="text-white">Password:</label><br>
                        {!!Form::password('password', ["required"=>"required",'class'=> 'form-control','id'=>'password','placeholder' => 'Digite su contraseña'])!!}
                        </div> 
                        <input type="checkbox" name="recordarme" id="recordarme">
                        <label for="recordarme"></label>
                        <p id="tex-recordarme">Recuérdame</p>
                       
                        <div class="form-group">
                        {!! Form::submit('',['id' => 'enviar']) !!}
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop