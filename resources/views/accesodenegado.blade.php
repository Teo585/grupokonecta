@extends('layouts.vista')
@section('titulo')<h1 id="titulo" style="font-family: Arial, Helvetica, sans-serif;"><center></center></h1>@stop

@section('content')
@include('alerts.request')

<div class="container">
  <div class="jumbotron">
    <h1>El usuario logueado no cumple con los permisos necesarios para esta página</h1>      
    <p>Por favor verifique los permisos asignados.</p>
  </div> 
</div>
<center><a href="https://www.grupokonecta.com/"><img src="images/konecta.png" style="border-radius:150px;cursor:pointer"></center></a>

@stop