@extends('layouts.vista')
@section('titulo')<h3 id="titulo"><center>Clientes</center></h3>@stop
@section('content')
  @include('alerts.request')
  {!!Html::script('js/clientes.js')!!}
	@if(isset($clientes))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'eliminar')
			{!!Form::model($clientes,['route'=>['cliente.destroy',$clientes->idCliente],'method'=>'DELETE'])!!}
		@else
			{!!Form::model($clientes,['route'=>['cliente.update',$clientes->idCliente],'method'=>'PUT'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'cliente.store','method'=>'POST'])!!}
	@endif
  

<div id='form-section' >
<input type="hidden" id="token" value="{{csrf_token()}}"/>
	<fieldset id="clientes-form-fieldset">	
    <div class="form-group" >
          {!!Form::label('nombreCliente', 'Nombre cliente', array('class' => 'col-sm-2 control-label'))!!}
          <div class="col-sm-10" >
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-pencil" style="width: 14px;"></i>
                  </span>             
                    {!!Form::text('nombreCliente',null,['class'=>'form-control','placeholder'=>'Ingresa el nombre del cliente'])!!}
                </div>
          </div>
    </div>
    <div class="form-group" >
          {!!Form::label('documentoCliente', 'Documento', array('class' => 'col-sm-2 control-label'))!!}
          <div class="col-sm-10" >
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-pencil" style="width: 14px;"></i>
                  </span>             
                    {!!Form::text('documentoCliente',null,['class'=>'form-control','placeholder'=>'Ingresa el correo del cliente'])!!}
                </div>
          </div>
    </div>
    <div class="form-group" >
          {!!Form::label('correoCliente', 'Correo', array('class' => 'col-sm-2 control-label'))!!}
          <div class="col-sm-10" >
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-pencil" style="width: 14px;"></i>
                  </span>             
                    {!!Form::text('correoCliente',null,['class'=>'form-control','placeholder'=>'Ingresa el correo del cliente'])!!}
                </div>
          </div>
    </div>
    
    <div class="form-group required" id='test'>
          {!! Form::label('direccionCliente', 'Direcci&oacute;n', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-pencil " style="width: 14px;"></i>
                </span>
                {!!Form::text('direccionCliente',null,['class'=>'form-control','placeholder'=>'Ingresa la dirección del cliente'])!!}
            </div>
        </div>
    </div>    
    </fieldset>
    <br>
	@if(isset($clientes))
 		@if(isset($_GET['accion']) and $_GET['accion'] == 'eliminar')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-primary","onclick"=>'validarFormulario(event);'])!!}
 	@endif
    {!! Form::close() !!}
    

<script type="text/javascript">
$('#start_date').datetimepicker(({
			format: "YYYY-MM-DD"
        }));
</script>


</div>
@stop